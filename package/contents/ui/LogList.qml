/*
 * list component for displaying logs entries for the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick 2.7
import org.kde.plasma.components 2.0 as PlasmaComponents

ListView
{
    id: logListRoot
    anchors.fill: parent

    highlightMoveDuration: 0


    property bool jumpLastEntry: true
    property int timeWidth
    property int priorityWidth
    property int commandWidth

    Component.onCompleted:
    {
        timeWidth = plasmoid.configuration.timeWidth
        priorityWidth = plasmoid.configuration.priorityWidth
        commandWidth = plasmoid.configuration.commandWidth
    }

    delegate: PlasmaComponents.ListItem
    {
        id: logItem

        function getMaxHeight()
        {
            return Math.max(timeLabel.contentHeight+10, priorityLabel.contentHeight+10, commandLabel.contentHeight+10, messageLabel.contentHeight+10)
        }

        SplitView
        {
            id: contents
            orientation: Qt.Horizontal
            anchors.left: parent.left
            anchors.right: parent.right
            height: getMaxHeight()

            Item
            {
                id: timeItem
                width: logListRoot.timeWidth
                Layout.minimumWidth : 40

                PlasmaComponents.Label
                {
                    id: timeLabel
                    anchors.fill: parent
                    anchors.margins : 5
                    verticalAlignment: Text.AlignVCenter
                    wrapMode: Text.Wrap
                    text: timestamp
                }
                onWidthChanged:
                {
                    logListRoot.timeWidth = width
                    plasmoid.configuration.timeWidth = width
                    width = Qt.binding(function() {return logListRoot.timeWidth})
                }

            }
            Item
            {
                id: priorityItem
                width: logListRoot.priorityWidth
                Layout.minimumWidth : 40

                PlasmaComponents.Label
                {
                    id: priorityLabel
                    anchors.fill: parent
                    anchors.margins : 5
                    verticalAlignment: Text.AlignVCenter
                    wrapMode: Text.Wrap
                    text: priority
                }
                onWidthChanged:
                {
                    logListRoot.priorityWidth = width
                    plasmoid.configuration.priorityWidth = width
                    width = Qt.binding(function() {return logListRoot.priorityWidth})
                }
            }
            Item
            {
                id: commandItem
                width: logListRoot.commandWidth
                Layout.minimumWidth : 40
                PlasmaComponents.Label
                {
                    id: commandLabel
                    anchors.fill: parent
                    anchors.margins : 5
                    wrapMode: Text.Wrap
                    text: command
                }
                onWidthChanged:
                {
                    logListRoot.commandWidth = width
                    plasmoid.configuration.commandWidth = width
                    width = Qt.binding(function() {return logListRoot.commandWidth})
                }
            }
            Item
            {
                id: messageItem
                Layout.minimumWidth : 40
                Layout.fillWidth : true
                PlasmaComponents.Label
                {
                    id: messageLabel
                    anchors.fill: parent
                    anchors.margins : 5
                    wrapMode: Text.Wrap
                    text: message
                }
            }
        }
    }

    onCountChanged:
    {
        if (jumpLastEntry)
        {
            currentIndex = count - 1
        }
    }
    onCurrentIndexChanged:
    {
        if (jumpLastEntry)
        {
            currentIndex = count - 1
        }
    }
}
