/*
 * Filter config panel for the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


import QtQuick 2.7
import QtQuick.Layouts 1.3
import org.kde.plasma.components 2.0 as Components

ColumnLayout
{
    id: filterConfigPage

    property alias cfg_localJournalOnly: localJournalOnlyCheckbox.checked
    property alias cfg_userJournal: userJournalCheckbox.checked
    property alias cfg_systemJournal: systemJournalCheckbox.checked

    Components.CheckBox
    {
        id: localJournalOnlyCheckbox
        text: i18n("Show only local journals")
    }
    Components.CheckBox
    {
        id: userJournalCheckbox
        text: i18n("Show the journal of the current user")
    }
    Components.CheckBox
    {
        id: systemJournalCheckbox
        text: i18n("Show the system journal")
    }
    Item
    {
        Layout.fillHeight: true
    }
}
