/*
 * Main qml file for the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.7
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.private.journalviewer 1.0


Item
{
    id: journalViewerApplet

    readonly property int cfgLogEntryCount: plasmoid.configuration.logEntryCount
    readonly property bool cfgJumpLastEntry: plasmoid.configuration.jumpLastEntry
    readonly property bool cfgLocalJournalOnly: plasmoid.configuration.localJournalOnly
    readonly property bool cfgUserJournal: plasmoid.configuration.userJournal
    readonly property bool cfgSystemJournal: plasmoid.configuration.systemJournal

    Journal
    {
        id: journal

        maxMessages: cfgLogEntryCount
        localJournalOnly: cfgLocalJournalOnly
        userJournal: cfgUserJournal
        systemJournal: cfgSystemJournal

    }

    Plasmoid.fullRepresentation: Item
    {

        id: root

        Layout.minimumWidth: units.gridUnit * 30
        Layout.minimumHeight: units.gridUnit * 8

        Journal
        {
            id: journal

            maxMessages: cfgLogEntryCount
            localJournalOnly: cfgLocalJournalOnly
            userJournal: cfgUserJournal
            systemJournal: cfgSystemJournal
        }


        PlasmaExtras.ScrollArea
        {
            id: scrollArea
            anchors.fill: parent
            LogList
            {
                id: loglist
                jumpLastEntry: journalViewerApplet.cfgJumpLastEntry
            }
            onVisibleChanged:
            {
                if (visible)
                {
                    // only set the log model after we a re visible. Helps to not slowdown the plasma startup that logs a lot to journald
                    loglist.model = journal.logMsgModel
                }
            }
        }
    }
}

