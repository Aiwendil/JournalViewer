/*
 * Journal class used in the QML of the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "journal.h"

#include <QDateTime>
#include <QDebug>

Journal::Journal(QObject *parent)
    : QObject(parent),
      m_localJournalOnly(true),
      m_userJournal(true),
      m_systemJournal(true),
      m_maxMessages(40),
      m_logMessages(new MessageListModel(this))
{
    openJournal();
}

Journal::~Journal()
{
    if (notify)
    {
        delete notify;
    }
    if (journal)
    {
        sd_journal_close(journal);
    }
    delete m_logMessages;
}

void Journal::openJournal()
{
    if (journal)
    {
        sd_journal_close(journal);
    }
    int flags = 0;
    if (m_localJournalOnly) flags |= SD_JOURNAL_LOCAL_ONLY;
    if (m_userJournal) flags |= SD_JOURNAL_CURRENT_USER;
    if (m_systemJournal) flags |= SD_JOURNAL_SYSTEM;
    int ret = sd_journal_open(&journal, flags);
    if (ret < 0)
    {
        journal = nullptr;
        qDebug() << "Error opening the journal: " << QString(strerror(-ret));
    }
    else
    {
        ret = sd_journal_seek_tail(journal);
        if (ret < 0)
        {
            qDebug() << "Error seeking the end of the journal: " << QString(strerror(-ret));
        }
        sd_journal_previous_skip(journal, m_maxMessages+1);

        readJournal();

        if (notify)
        {
            delete notify;
            notify = nullptr;
        }
        notify = new QSocketNotifier(sd_journal_get_fd(journal), QSocketNotifier::Read);
        connect(notify, &QSocketNotifier::activated, this, &Journal::processJournal);
        notify->setEnabled(true);
        // TODO: figure out why this leaves the journal in SD_JOURNAL_INVALIDATE the first time in Journal::processJournal()

    }
}

void Journal::readJournal()
{
    if (journal)
    {
        while (sd_journal_next(journal) != 0)
        {
            QString msg;
            QString time;
            QString pri;
            QString com;
            const char *d;
            size_t l;
            int ret;

            ret = sd_journal_get_data(journal, "MESSAGE", (const void **)&d, &l);
            if (ret < 0)
            {
                msg = "";
            }
            else
            {
                msg = QString(d).mid(8);
            }

            ret = sd_journal_get_data(journal, "PRIORITY", (const void **)&d, &l);
            if (ret < 0)
            {
                pri = "";
            }
            else
            {
                int priNumber = QString(d).mid(9).toInt();
                switch (priNumber)
                {
                    case 0:
                        pri="emerg";
                        break;
                    case 1:
                        pri="alert";
                        break;
                    case 2:
                        pri="crit";
                        break;
                    case 3:
                        pri="err";
                        break;
                    case 4:
                        pri="warning";
                        break;
                    case 5:
                        pri="notice";
                        break;
                    case 6:
                        pri="info";
                        break;
                    case 7:
                        pri="debug";
                        break;
                    default:
                        pri="";
                }
            }

            ret = sd_journal_get_data(journal, "_EXE", (const void **)&d, &l);
            if (ret < 0)
            {
                com = "";
            }
            else
            {
                com = QString(d).mid(5);
            }

            ret = sd_journal_get_data(journal, "_SOURCE_REALTIME_TIMESTAMP", (const void **)&d, &l);
            if (ret < 0)
            {
                time="";
            }
            else
            {
                QDateTime timeSt;
                timeSt.setMSecsSinceEpoch(QString(d).mid(27).toLong()/1000);
                time = timeSt.toString();
            }
            m_logMessages->addMessage(LogMessage(msg, pri, com, time));
        }
    }
}

void Journal::reReadJournal()
{
    if (journal)
    {
        int ret = sd_journal_seek_tail(journal);
        if (ret < 0)
        {
        }
        sd_journal_previous_skip(journal, m_maxMessages+1);
        readJournal();
    }
}

void Journal::processJournal()
{
    if (journal)
    {
        int status = sd_journal_process(journal);
        switch (status)
        {
            case SD_JOURNAL_NOP:
                break;
            case SD_JOURNAL_INVALIDATE:
                reReadJournal();
                break;
            case SD_JOURNAL_APPEND:
                readJournal();
                break;
            default:
                qDebug() << "Error processing journal";
        }
    }
}


void Journal::setLocalJournalOnly(const bool value)
{
    if (m_localJournalOnly != value)
    {
        m_localJournalOnly = value;
        openJournal();
        emit localJournalOnlyChanged();
    }
}

void Journal::setUserJournal(const bool value)
{
    if (m_userJournal != value)
    {
        m_userJournal = value;
        openJournal();
        emit userJournalChanged();
    }
}

void Journal::setSystemJournal(const bool value)
{
    if (m_systemJournal != value)
    {
        m_systemJournal = value;
        openJournal();
        emit systemJournalChanged();
    }
}

void Journal::setMaxMessages(const int value)
{
    if (m_maxMessages != value)
    {
        m_maxMessages = value;
        m_logMessages->setMaxMessages(value);
        reReadJournal();
        emit maxMessagesChanged();

    }
}
