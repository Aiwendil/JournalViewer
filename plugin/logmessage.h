/*
 * helper-class for a log message for the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef LOGMESSAGE_H
#define LOGMESSAGE_H

#include <QString>

class LogMessage
{
    public:
        LogMessage()
        {
        }
        LogMessage(const QString &newMsg, const QString &newPri, const QString &newCom, const QString &newTime)
            : m_Message(newMsg),
              m_Priority(newPri),
              m_Command(newCom),
              m_Timestamp(newTime)
        {
        }
        LogMessage(const LogMessage&) = default;
        LogMessage& operator=(const LogMessage&) = default;
        QString message() const { return m_Message; }
        QString priority() const { return m_Priority; }
        QString command() const { return m_Command; }
        QString timestamp() const { return m_Timestamp; }

    private:
        QString m_Message;
        QString m_Priority;
        QString m_Command;
        QString m_Timestamp;
};

#endif // LOGMESSAGE_H
