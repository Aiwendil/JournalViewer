/*
 * MessageListModel class used in the qml ListView of the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "messagelistmodel.h"

namespace
{
    enum
    {
        MessageRole = Qt::UserRole,
        TimestampRole,
        PriorityRole,
        CommandRole
    };
}

MessageListModel::MessageListModel(QObject *parent)
    : QAbstractListModel(parent),
      m_maxMessages(40)
{
}

QHash<int, QByteArray> MessageListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[MessageRole] = "message";
    roles[TimestampRole] = "timestamp";
    roles[PriorityRole] = "priority";
    roles[CommandRole] = "command";

    return roles;
}

QVariant MessageListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || (index.row() >= m_messages.size()))
    {
        return QVariant();
    }

    const LogMessage msg = m_messages.at(index.row());

    switch (role)
    {
        case MessageRole: return msg.message();
        case TimestampRole: return msg.timestamp();
        case PriorityRole: return msg.priority();
        case CommandRole: return msg.command();
    }
    return QVariant();
}

int MessageListModel::rowCount(const QModelIndex &index) const
{
    if (! index.isValid())
    {
        return m_messages.size();
    }

    return 0;
}

void MessageListModel::setMaxMessages(const int maxMessages)
{
    m_maxMessages = maxMessages;
    trimList();
};

void MessageListModel::addMessage(const LogMessage &msg)
{
    beginInsertRows(QModelIndex(), m_messages.size(), m_messages.size());
    m_messages.push_back(msg);
    endInsertRows();
    trimList();
}


void MessageListModel::trimList()
{
    int columnsToRemove = m_messages.length() - m_maxMessages;
    if (columnsToRemove > 0)
    {
        beginRemoveRows(QModelIndex(), 0, columnsToRemove-1);
        for (int i = 0; i < columnsToRemove; i++)
        {
            m_messages.removeFirst();
        }
        endRemoveRows();
    }
}
