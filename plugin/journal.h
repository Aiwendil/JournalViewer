/*
 * Journal class used in the QML of the JournalViewer Plasmoid
 * Copyright (C) 2016  Aiwendil <AiwendilH@googlemail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef JOURNAL_H
#define JOURNAL_H

#include "logmessage.h"
#include "messagelistmodel.h"
#include <systemd/sd-journal.h>
#include <QSocketNotifier>


class Journal : public QObject
{
    Q_OBJECT

    Q_PROPERTY(MessageListModel* logMsgModel READ logMsgModel CONSTANT)
    Q_PROPERTY(bool localJournalOnly READ localJournalOnly WRITE setLocalJournalOnly NOTIFY localJournalOnlyChanged)
    Q_PROPERTY(bool userJournal READ userJournal WRITE setUserJournal NOTIFY userJournalChanged)
    Q_PROPERTY(bool systemJournal READ systemJournal WRITE setSystemJournal NOTIFY systemJournalChanged)
    Q_PROPERTY(int maxMessages READ maxMessages WRITE setMaxMessages NOTIFY maxMessagesChanged)

    public:
        Journal(QObject *parent = nullptr);
        ~Journal();
        Journal(const Journal&) = delete;
        Journal& operator=(const Journal&) = delete;

        void setLocalJournalOnly(const bool value);
        bool localJournalOnly() const { return m_localJournalOnly; }
        void setUserJournal(const bool value);
        bool userJournal() const { return m_userJournal; }
        void setSystemJournal(const bool value);
        bool systemJournal() const { return m_systemJournal; }
        void setMaxMessages(const int value);
        int maxMessages() const { return m_maxMessages; }
        MessageListModel* logMsgModel() const { return m_logMessages; };

    private:
        void openJournal();
        void readJournal();
        void reReadJournal();

    signals:
        void localJournalOnlyChanged();
        void userJournalChanged();
        void systemJournalChanged();
        void recordChanged();
        void maxMessagesChanged();

    private slots:
        void processJournal();

    private:
        bool m_localJournalOnly;
        bool m_userJournal;
        bool m_systemJournal;
        int m_maxMessages;
        MessageListModel *m_logMessages;
        sd_journal *journal = nullptr;
        QSocketNotifier *notify = nullptr;
};

#endif // JOURNAL_H
